\documentclass[a4paper]{article}

\usepackage{markdown}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{hyperref}
\usepackage{amssymb}
\usepackage{outline} \usepackage{pmgraph} \usepackage[normalem]{ulem}
\usepackage{graphicx} \usepackage{verbatim}
% \usepackage{minted} % need `-shell-escape' argument for local compile

\title{
    \vspace*{1in}
    \includegraphics[width=7in]{figures/advertisement} \\
    \vspace*{1.2in}
    \textbf{\huge Project Report}
    \vspace{0.2in}
}

\author{Chloe Henderson}
\date{}

\begin{document}

\maketitle
\setcounter{page}{0}
\thispagestyle{empty}
\newpage


\section*{Goals}

\begin{itemize}
\item Make a Sudoku solver in Haskell
\end{itemize}

Sub-goals:

\begin{itemize}
\item Learn Haskell
\item Get back into the functional programming headspace
\end{itemize}

\section*{Tools and Resources}

\begin{itemize}
\item Git
\item Haskell/GHC/GHCi
\item Windows Subsystem for Linux
\item \textit{Haskell Programming from first principles}
\item \textit{Learn You a Haskell for Great Good!}
\item Stack Overflow
\end{itemize}

\section*{Terminology}

\begin{itemize}
\item \textbf{Sudoku:} A puzzle defined by a grid with givens and a unique solution, which can be found by completing the grid according to the rule.
\item \textbf{The Rule:} The one rule of Sudoku: complete the grid in such a way that each row, column, and box contains the digits 1 through 9.
\item \textbf{Puzzle:} Any incomplete Sudoku grid which follows the rule. There may be one or more solutions.
\item \textbf{Solution:} The answer to the puzzle. A grid where each cell contains a digit between 1 and 9 without violating the rule.
\item \textbf{Contradiction:} The state of the grid in which the puzzle no longer complies with the rule.
\item \textbf{Box:} A group of 9 cells in a 3x3 formation.
\item \textbf{Cell:} A small square in a Sudoku grid that can contain a single digit.
\item \textbf{Column:} A vertical line in the grid, containing 9 cells.
\item \textbf{Given:} A digit placed in a cell by the puzzle setter. It cannot be changed by the player.
\item \textbf{Grid:} 81 cells organized in 9 rows and 9 columns.
\item \textbf{House:} A group of 9 cells that must contain digits 1 through 9. A house can be a row, a column, or a box in standard Sudoku.
\item \textbf{Peer:} A cell that shares a house with another cell.
\item \textbf{Row:} A horizontal line in the grid, containing 9 cells.
\item \textbf{Value:} A number between 1 and 9 which can be placed into a cell.
\item \textbf{Backtracking:} Return to an earlier position in the solving path, after a contradiction or a dead end has been reached.
\item \textbf{Candidate:} A possible digit that may be placed into a cell.
\item \textbf{Eliminate:} Remove a candidate from the grid.
\item \textbf{Guess:} A digit placed into a cell without a logical reason.
\item \textbf{Forced Digit/Naked Single:} A cell with only a single candidate remaining.
\item \textbf{Pinned Digit/Hidden Single:} A digit which is only a candidate for a single cell in a house.
\item \textbf{Placement:} The placing of a candidate into an unsolved cell, thus changing it to a solved cell.
\end{itemize}

Source: \href{http://sudopedia.enjoysudoku.com/Terminology.html}{http://sudopedia.enjoysudoku.com/Terminology.html}

\section*{Design and Algorithm Overview}

\begin{itemize}
\item \texttt{(r, c)} denotes the cell at row \texttt{r} and column \texttt{c}
	\begin{itemize}
    \item Rows and columns indexed at 1
    \item Top left cell is \texttt{(1, 1)}
	\end{itemize}
\item Unsolved cells have a value of 0
\item Cells with conflicts (no candidates or multiple hidden singles) have a value of -1
\item Solving technique: singles and guessing
	\begin{itemize}
    \item Short version: Given a puzzle with at least some givens/placements, keep placing singles until the rule is violated (won't happen for valid puzzles) or no additional placements can be made, then make a guess and start from the top
	\end{itemize}
\end{itemize}

\section*{Singles Implementation}

\textbf{Naked single:} cell with only one candidate.
\begin{itemize}
\item From the pool of all allowable values, filter out the values that appear in one of the cell's peers
	\begin{itemize}
    \item No candidates $\rightarrow$ Conflict
    \item One candidate $\rightarrow$ Naked single, make placement
    \item Multiple candidates $\rightarrow$ Check for hidden single(s)
	\end{itemize}
\end{itemize}
\textbf{Hidden single:} value which is only a candidate for a single in a house
\begin{itemize}
\item From the list of candidates, filter out the values that are candidates for one of the cell's peers
	\begin{itemize}
    \item No hidden singles $\rightarrow$ Cell remains unsolved
    \item One hidden single $\rightarrow$ Valid hidden single, make placement
    \item Multiple hidden single $\rightarrow$ Conflict (multiple values can only be in this cell)
	\end{itemize}
\end{itemize}

\section*{Singles: Helpful Functions}

\begin{itemize}
\item \texttt{lookupValue}: Given a puzzle and a cell, looks up the cell's value in the puzzle and returns 0 if it isn't found
	\begin{itemize}
    \item Input puzzles are basically sparse matrices, so the first lookup takes advantage of the case where a cell isn't found
	\end{itemize}
\item \texttt{getRow}, \texttt{getColumn}, \texttt{getBox}, \texttt{getAllHouses}: Given a list of allowable coordinates and a cell, get all the cells in the same row, column, box, or union of the three houses, respectively
\item \texttt{getPossibleValuesAllCells}: Given a list of allowable coordinates and a puzzle, maps all of the cells in the puzzle's grid to a list that contains their value (if they have one) or a list of their candidates (if they don't)
	\begin{itemize}
    \item Helps with hidden singles: look up values/candidates for the cell's peers, and any value that isn't found anywhere else in a house must be in that cell
    \item \texttt{lookupCandidates} takes the above list and a cell and returns that cell's candidate/value list
	\end{itemize}
\end{itemize}

\section*{Main Solving Function}

\begin{itemize}
\item Ignore puzzles with no givens
\item Given a non-empty puzzle, place all discovered singles
	\begin{itemize}
    \item If the resulting puzzle violates the one rule, return an empty grid
    \item If the resulting puzzle contains a conflict, return an empty grid
    \item If the resulting puzzle has no unsolved cells left, return it! It's solved!
    \item If the resulting puzzle has fewer unsolved cells than the given puzzle, start from the top (placing all discovered singles) and return the result
    \item Otherwise, make a guess
    	\begin{itemize}
        \item Get all of the unsolved cells in the resulting puzzle, as well as the candidates for each of these cells: each unsolved cell-candidate pairing is a guess
        \item Create a list of all the puzzles that result from placing one guess in the resulting puzzle from above and attempt to solve each of them from the top
    	\end{itemize}
	\end{itemize}
\end{itemize}

\section*{Difficulties}

\begin{itemize}
\item Struggled to get back into the functional programming paradigm, break down the problem, and use my time effectively
\item Struggled with type errors, general lack of familiarity with Haskell
\item Simplicity of solver resulted in a nasty bug that wasn't fixed until late in production (i.e. the day before my presentation)
\item Windows and the Microsoft Office Suite can be a pain sometimes
\end{itemize}

\section*{Feature Wish List}

\begin{itemize}
\item Implement more sophisticated solving techniques so that the solver performs better in general and on more difficult puzzles
\item Add a GUI for inputting puzzles and showing solutions
\item Add puzzle generation capabilities and difficulty selection
\item Add more robust and customizable file I/O
\end{itemize}

\section*{Special Thanks}

\begin{itemize}
\item Professor Amtoft
	\begin{itemize}
    \item Got the ball rolling for me by writing a partial simple solver in SML
	\end{itemize}
\item Devereux Henley
	\begin{itemize}
    \item Helped me with refactoring, type errors, and a function or two
	\end{itemize}
\item Daniel Bramucci
	\begin{itemize}
    \item Helped me with a ton of refactoring and fixing the massive bug
	\end{itemize}
\end{itemize}

\section*{Instructions}

\subsection*{How to Use}
\begin{markdown}
1. Navigate to the directory containing `solver.hs`
2. Compile it: `ghc solver.hs`
3. Run it: `./solver <input file name> <output file name>`
\end{markdown}

\subsection*{Input File Format}

\begin{markdown}
Line 1: The size of the grid (i.e. 4 or 9).

All other lines: A description of a given value in the format `<row number> <column number> <value>`

Note: The rows and columns are indexed at 1, and `(1, 1)` is the top left corner.
\end{markdown}

\subsection*{Example Input File}

\begin{markdown}
    4
    1 1 1
    2 3 2
    3 2 3
    4 4 4

This would produce the following puzzle:

\end{markdown}

\begin{center}
\begin{tabular}{ |c|c|c|c| } 
\hline
1 &   &   &   \\
\hline
  &   & 2 &   \\
\hline
  & 3 &   &   \\
\hline
  &   &   & 4 \\
\hline
\end{tabular}
\end{center}

\subsection*{Output File Format}

A CSV file containing the values from the solution, with one row per line. The CSV file will be created in a directory of the same name.

\subsection*{Example Output File}

\begin{markdown}

    1,2,4,3
    3,4,2,1
    4,3,1,2
    2,1,3,4

This encodes the solution to the above puzzle:

\end{markdown}

\begin{center}
\begin{tabular}{ |c|c|c|c| } 
\hline
1 & 2 & 4 & 3 \\
\hline
3 & 4 & 2 & 1 \\
\hline
4 & 3 & 1 & 2 \\
\hline
2 & 1 & 3 & 4 \\
\hline
\end{tabular}
\end{center}

\end{document}