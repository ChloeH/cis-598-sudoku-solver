# Simple Sudoku and Shidoku Solver in Haskell

## How to Use
1. Navigate to the directory containing `solver.hs`
2. Compile it: `ghc solver.hs`
3. Run it: `./solver <input file name> <output file name>`

## Input File Format

Line 1: The size of the grid (i.e. 4 or 9).

All other lines: A description of a given value in the format `<row number> <column number> <value>`

Note: The rows and columns are indexed at 1, and `(1, 1)` is the top left corner.

### Example Input File

    4
    1 1 1
    2 3 2
    3 2 3
    4 4 4

This would produce the following puzzle:

<table>
    <tr>
        <td>1</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>2</td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td>3</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>4</td>
    </tr>
</table>

## Output File Format

A CSV file containing the values from the solution, with one row per line. The CSV file will be created in a directory of the same name.

### Example Output File

    1,2,4,3
    3,4,2,1
    4,3,1,2
    2,1,3,4

This encodes the solution to the above puzzle:

<table>
    <tr>
        <td>1</td>
        <td>2</td>
        <td>4</td>
        <td>3</td>
    </tr>
    <tr>
        <td>3</td>
        <td>4</td>
        <td>2</td>
        <td>1</td>
    </tr>
    <tr>
        <td>4</td>
        <td>3</td>
        <td>1</td>
        <td>2</td>
    </tr>
    <tr>
        <td>2</td>
        <td>1</td>
        <td>3</td>
        <td>4</td>
    </tr>
</table>
