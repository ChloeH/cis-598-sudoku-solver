import Control.Monad
import Data.List -- Has a function for removing duplicates from a list
import System.Directory
import System.Environment
import Data.Maybe (fromMaybe, fromJust)


type Cell = (Int, Int)
type Value = Int
type Puzzle = [(Cell, Value)]


-- Checks whether the given board with the given coordinates is valid (satisfies
-- the one rule - Thanks, Danny B!)
isValid :: [Int] -> Puzzle -> Bool
isValid coords puzzle = valid
    where 
        normalCells = filter (\(_, v) -> v >= 1) puzzle
        valid = all 
            (\cell -> (all
                        (\house -> allUnique
                                . filter (`notElem` [-1, 0])
                                . map (lookupValue puzzle)
                                . house coords 
                                $ cell)
                        [getRow, getColumn, getBox])) 
            (getGrid coords)
        
        allUnique :: Eq a => [a] -> Bool
        allUnique xs = 
            all (\xis -> length xis == 1) . map (`elemIndices` xs) $ xs
          

-- Wrapper for looking up the given cell's value in the given puzzle
lookupValue :: Puzzle -> Cell -> Value
lookupValue cellValues cell = fromMaybe 0 (lookup cell cellValues)


-- Wrapper for looking up the given cell's candidates
-- If fromJust fails, there is a problem with the program itself, so crashing is
-- correct
lookupCandidates :: [(Cell, [Value])] -> Cell -> [Value]
lookupCandidates cellCandidates cell = fromJust (lookup cell cellCandidates)


-- Gets all of the cells in the grid with the given coordinate range
getGrid :: [Int] -> [Cell]
getGrid coords = [(row, column) | row <- coords, column <- coords]

-- Gets all the cells in the same row as the given cell
getRow :: [Int] -> Cell -> [Cell]
getRow coords (row, _) = [(row, column) | column <- coords]

-- Gets all the cells in the same column as the given cell
getColumn :: [Int] -> Cell -> [Cell]
getColumn coords (_, column) = [(row, column) | row <- coords]

-- Gets all the cells in the same box as the given cell
getBox :: [Int] -> Cell -> [Cell]
getBox coords (row, column) = getCoords rowCoords columnCoords
    where
        boxSize = if length coords == 4 then 2 else 3
        rowCoords = getCoordRange boxSize row
        columnCoords = getCoordRange boxSize column
        -- Gets the coordinate range for the box with the given size containing
        -- a cell with the given coordinate (row or column, doesn't matter)
        getCoordRange :: Int -> Int -> [Int]
        getCoordRange boxSize coord = [min..max]
            where
                multiple = ceiling (fromIntegral coord / fromIntegral boxSize)
                max = multiple * boxSize
                min = ((multiple - 1) * boxSize) + 1
        -- Gets the Cartesian product of the sets of row and column coordinates
        getCoords :: [Int] -> [Int] -> [Cell]
        getCoords rowCoords columnCoords = 
            [(row, column) | row <- rowCoords, column <- columnCoords]


-- Gets all the cells in the same row, column, or box as the given cell
getAllHouses :: [Int] -> Cell -> [Cell]
getAllHouses coords cell = 
    concat [getRow coords cell, getColumn coords cell, getBox coords cell]


-- Gets all the cell-value pairs that have the given value
getCellsWithValue :: Puzzle -> Value -> [(Cell, Value)]
getCellsWithValue puzzle value = filter (\(_, v) -> v == value) puzzle


-- Gets all the unsolved cells in the given puzzle
getUnsolvedCells :: Puzzle -> [Cell]
getUnsolvedCells puzzle = map fst (getCellsWithValue puzzle 0)

-- Gets the number of conflicted cells in the given puzzle
getConflictedCellsCount :: Puzzle -> Int
getConflictedCellsCount puzzle = length $ getCellsWithValue puzzle (-1)

-- Gets the cell-value pairs for all the possible guesses for the given
-- cell in the given puzzle
getCellGuesses :: [Int] -> Puzzle -> Cell -> [(Cell, Value)]
getCellGuesses coords puzzle cell = map
    (\guess -> (cell, guess))
    (lookupCandidates (getPossibleValuesAllCells coords puzzle) cell)


-- Maps the cells in the given puzzle to a list of their candidates or their
-- value
getPossibleValuesAllCells :: [Int] -> Puzzle -> [(Cell, [Value])]
getPossibleValuesAllCells coords puzzle = 
    map 
        (\cell -> (cell, getValueList cell))
        (getGrid coords)
    where
        -- Get a list containing either the value of the cell or its candidates
        getValueList :: Cell -> [Value]
        getValueList cell
            | val == 0  = getCandidates cell
            | otherwise = [val]
            where val = lookupValue puzzle cell
        
        -- Get a list of the candidates for the given cell
        getCandidates :: Cell -> [Value]
        getCandidates cell = filter (`notElem` taken) coords
                where taken = map (lookupValue puzzle) (getAllHouses coords cell)

-- If the given cell is a clue, gets its value; otherwise, attempts to solve it
-- and returns the value if a placement was made or 0 if the cell could not be
-- solved
solveCell :: [Int] -> Puzzle -> Cell -> Value
solveCell coords puzzle cell = 
    case cellCandidates of 
      []          -> -1 -- Conflict: no candidates
      [naked]     -> naked -- Naked single
      candidates  -> case filter isHiddenSingle candidates of 
                       []       -> 0      -- No forced value
                       [hidden] -> hidden -- Hidden single
                       _        -> -1     -- Conflict: multiple hidden singles
    where 
        -- The candidates for the cell
        cellCandidates = lookupCandidates cellsPossibleValues cell

        -- All cells' possible values
        cellsPossibleValues = getPossibleValuesAllCells coords puzzle

        -- Excludes the cell being solved from the lookup
        lookupExcludeCell :: Cell -> [Value]
        lookupExcludeCell c
            | c == cell = [] 
            | otherwise = lookupCandidates cellsPossibleValues c
        
        -- The candidates for all of the other cells in the same row
        rowCandidates = concatMap lookupExcludeCell (getRow coords cell)
        -- The candidates for all of the other cells in the same column
        columnCandidates = concatMap lookupExcludeCell (getColumn coords cell)
        -- The candidates for all of the other cells in the same box
        boxCandidates = concatMap lookupExcludeCell (getBox coords cell)
        -- Gets whether the given value is a hidden single
        isHiddenSingle :: Value -> Bool
        isHiddenSingle v = 
            any (v `notElem`) [rowCandidates, columnCandidates, boxCandidates]

-- Finds all solutions to the given puzzle with the given coordinate range
solvePuzzle :: [Int] -> Puzzle -> [Puzzle]
solvePuzzle _ [] = []
solvePuzzle coords puzzle
    | not (isValid coords reducedPuzzle)       = [] -- Puzzle violates the rule
    | hasConflict                              = [] -- Puzzle was unsolvable
    | numUnsolvedCellReduced == 0              = [reducedPuzzle] -- Puzzle solved!
    | numUnsolvedCellReduced < numUnsolvedCell = solvePuzzle coords reducedPuzzle
    | otherwise = concatMap (solvePuzzle coords) reducedGuessResults -- Guess
    where
        -- The result of placing forced values in the given puzzle
        reducedPuzzle = map 
            (\cell -> (cell, solveCell coords puzzle cell)) 
            (getGrid coords)

        hasConflict = getConflictedCellsCount reducedPuzzle /= 0

        numUnsolvedCell = length (getUnsolvedCells puzzle)
        unsolvedReduced = getUnsolvedCells reducedPuzzle
        numUnsolvedCellReduced = length unsolvedReduced

        guesses = concatMap (getCellGuesses coords reducedPuzzle) unsolvedReduced
        reducedGuessResults =  [guess : reducedPuzzle | guess <- guesses]
        
-- Solves the given Shidoku puzzle
solveShidoku :: Puzzle -> [Puzzle]
solveShidoku puzzle = nub (solvePuzzle [1..4] puzzle)

-- Solves the given Sudoku puzzle
solveSudoku :: Puzzle -> [Puzzle]
solveSudoku puzzle = nub (solvePuzzle [1..9] puzzle)

-- Extracts the grid size and the puzzle's givens from the given string
parsePuzzle :: String -> (Int, Puzzle)
parsePuzzle fileContents = (gridSize, cells)
    where
        puzzleInfo = lines fileContents
        
        gridSize = read (head puzzleInfo) :: Int
        rawCellInfo = map words (tail puzzleInfo)
        cells = map getCellValueFromList rawCellInfo
        -- Gets a cell-value pair from the given list
        getCellValueFromList :: [String] -> (Cell, Value)
        getCellValueFromList [row, col, val] = ((read row, read col), read val)

-- Writes the given solutions to files with the given word in their name
writeSolutions :: String -> [Puzzle] -> Int -> IO ()
writeSolutions fileName solutions gridSize = do 
    createDirectoryIfMissing True fileName
    zipWithM_ writeFile fileNames fileContents
    where
        fileContents = map (showSolution gridSize) solutions
        
        -- Uses the given grid size to get a CSV-friendly string representing
        -- the given puzzle solution
        showSolution :: Int -> Puzzle -> String
        showSolution gridSize solution = concatMap showCellValue solution
            -- Gets the CSV string of the given cell-value pair
            where showCellValue :: (Cell, Value) -> String
                  showCellValue ((_, col), val) = 
                      if col < gridSize then show val ++ ","
                      else show val ++ "\r\n"
        
                      
        fileNames = getFileNames fileName ".csv" (length solutions)

        -- Gets the given number of unique file names with the given extension
        -- and the given prefix
        getFileNames :: String -> String -> Int -> [String]
        getFileNames pref ext num = 
            [pref ++ "/" ++ pref ++ "-" ++ show n ++ ext | n <- [1..num]]

main = do
    (inFileName : outFileName : _) <- getArgs
    fContents <- readFile inFileName
    let (gridSize, puzzle) = parsePuzzle fContents
        solve = if gridSize == 9 then solveSudoku else solveShidoku
        solutions = solve puzzle
    writeSolutions outFileName solutions gridSize

