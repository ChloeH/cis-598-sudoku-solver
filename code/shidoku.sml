exception Misc

exception NoSolution

open List

type Point = int * int

val initial_solution3 = fn p => case p of
  (1,1) => 1
| (2,3) => 2
| (3,2) => 3
|   _   => 0

val initial_solution4 = fn p => case p of
  (1,1) => 1
| (2,3) => 2
| (3,2) => 3
| (4,4) => 4
|   _   => 0


(*
1 0 0 0
0 0 2 0
0 3 0 0
0 0 0 4
*)

fun lookup [] p = raise Misc
|   lookup ((p1,v1) :: dict) p =
     if p = p1 then v1
     else lookup dict p

val allPoints = concat (map (fn x => [(x,1),(x,2),(x,3),(x,4)]) [1,2,3,4])

fun mk_horizontal (x,_) = map (fn y => (x,y)) [1,2,3,4]

fun mk_vertical (_,y) = map (fn x => (x,y)) [1,2,3,4]

fun mk_square (x,y) = 
   if x > 2 then
      if y > 2 
        then [(3,3),(3,4),(4,3),(4,4)]
        else [(3,1),(3,2),(4,1),(4,2)]
      else if y > 2
        then [(1,3),(1,4),(2,3),(2,4)]
        else [(1,1),(1,2),(2,1),(2,2)]

fun mk_conflict p = concat [mk_horizontal p, mk_vertical p, mk_square p]

fun find_options solution p =
   let val v = solution p 
    in if v = 0
       then let val taken = (map solution (mk_conflict p))
             in filter (fn c => not (exists (fn n => n = c) taken)) [1,2,3,4]
            end
       else [v]
    end

fun mk_options solution =
   map (fn p => (p, find_options solution p)) allPoints

fun expand_sol options p =
   let val choices = lookup options p
       fun call_option p1 = if p1 = p then [] else lookup options p1
       val horizontal_choices = concat (map call_option (mk_horizontal p))
       val vertical_choices = concat (map call_option (mk_vertical p))
       val square_choices = concat (map call_option (mk_square p))
       fun is_forced n = 
              (all (fn m => n <> m) horizontal_choices) orelse
              (all (fn m => n <> m) vertical_choices) orelse
              (all (fn m => n <> m) square_choices) 
    in case choices of 
      [] => raise NoSolution
    | [n] => n
    | ns => case (filter is_forced ns) of
          [] => 0
        | [n] => n
        | ns => raise NoSolution
   end

fun expand_solution solution = let 
      val options = mk_options solution
     in map (fn p => (p,expand_sol options p)) allPoints
    end
